﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace WordDistance
{
    class Program
    {
        /// <summary>Finds the smallest distance between words in a string
        /// <param name="words">A string.</param>
        /// <param name="word1">The first word.</param>
        /// <param name="word2">The second word.</param>
        /// </summary>

        static void Distance(string words, string word1, string word2)
        {
            int min = Int32.MaxValue;
            string words_lower = words.ToLower(); // For cases where one of the words could be the first in the sentence

            List<string> wordsList = words_lower.Split(' ').ToList();
            List<int> word1Num = new List<int>();
            List<int> word2Num = new List<int>();

            // Check that the words are in the array
            if (!(wordsList.IndexOf(word1) > -1 && wordsList.IndexOf(word2) > -1))
            {
                Console.WriteLine("At least one of the words is not in the array!");
                return;
            }

            // Get indexes of first word
            while (wordsList.IndexOf(word1) != -1)
            {
                word1Num.Add(wordsList.IndexOf(word1));
                wordsList.Remove(word1);
                wordsList.Insert(word1Num[word1Num.Count - 1], "*");
            }

            // Get indexes of second word
            while (wordsList.IndexOf(word2) != -1)
            {
                word2Num.Add(wordsList.IndexOf(word2));
                wordsList.Remove(word2);
                wordsList.Insert(word2Num[word2Num.Count - 1], "*");
            }

            // Find the smallest distance
            foreach (int i in word1Num)
            {
                foreach (int j in word2Num)
                {
                    if (i > j) continue;
                    if (j - i < min) min = j - i - 1;
                }
            }

            if (min == Int32.MaxValue)
                Console.WriteLine("First word does not come before second word.");
            else
                Console.WriteLine("The minimum distance between the two words is {0}.", min);

            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            string words = "dog cat hello cat dog dog hello cat world";
            Distance(words, "hello", "world");

            string new_string = "At Epic, we have a few simple coding standards and conventions. This document is not meant to be a discussion or work in progress, but rather, reflects the state of Epic's current coding standards. Following the coding standards is mandatory.";
            Distance(new_string, "standards", "coding");
        }
    }
}
